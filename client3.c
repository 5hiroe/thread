#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <pthread.h>


#define NB_THREAD 5
#define CONNECT "Connecté"

int getPort (int argc, char * argv[]);
void * thread_1(int sock, char buffer_rcv[256], char buffer[256]);

void main (int argc, char * argv[]){


    int port = getPort (argc, argv);
    printf("Port starting on %i\n", port);


    int sock = socket(AF_INET, SOCK_STREAM, 0);
    
    struct sockaddr_in server;
    server.sin_addr.s_addr = inet_addr("127.0.0.1");/* =  */
    server.sin_family = AF_INET; /* On veut que ça discute en local */
    server.sin_port = htons(port); /* Port numéro *saisi par l'utilisateur*  */

    socklen_t size = sizeof(server);
    connect(sock, (struct sockaddr*)&server, size);

    int boucle = 1;
    char buffer[256];
    char buffer_rcv[256];



    pthread_t threads[NB_THREAD];

    int a[NB_THREAD];
    for (int i=0; i<NB_THREAD; i++){
        a[i] = i;
        pthread_create(&threads[i], NULL, thread_1(sock, buffer_rcv, buffer), &a[i]);
    }

    for (int i=0; i<NB_THREAD; i++){
        pthread_join(threads[i], NULL);
    }
}

void * thread_1(int sock, char buffer_rcv[256], char buffer[256]){

        while (1==1){ 
            recv(sock, buffer_rcv, sizeof(* buffer_rcv), 0);
                if(buffer != buffer_rcv){
                    printf("%s", buffer_rcv);
                }
            fgets(buffer, sizeof(* buffer), stdin);
            send(sock, buffer, sizeof(* buffer), 0);
        }
       
}

int getPort (int argc, char * argv[]){
    if (argc < 2){
        printf("Missing argument port!\n");
        exit(EXIT_FAILURE);
    }
    return atoi(argv[1]);
}
