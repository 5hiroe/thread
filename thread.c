#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <pthread.h>


#define NB_THREAD 2

int getPort (int argc, char * argv[]);
void * thread_1(int sock, struct sockaddr_in server, char buffer_rcv[256]);

void main (int argc, char * argv[]){


    int port = getPort (argc, argv);
    printf("Port starting on %i\n", port);


    char buffer_rcv[256];
    char buffer[256];
    char buffer_rcv2[256];
/* Création de la socket */
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    /* Si la socket est négative on affiche un message d'erreur */
    if (sock<0){
        printf("ça marche pas wlh");
    }
/* On veut discuter sur cette adresse ip (C'est la mienne ^^) */
    inet_addr("127.0.0.1");

/* On paramètre la socket */
    struct sockaddr_in server;
    server.sin_addr.s_addr = htonl(INADDR_ANY);/* = N'importe quelle adresse */
    server.sin_family = AF_INET; /* On veut que ça discute en local */
    server.sin_port = htons(port); /* Port numéro *saisi par l'utilisateur* */
    
    bind(sock, (struct sockaddr*)&server, sizeof(server));

    listen(sock, 6); /* On crée la fonction listen qui va écouter sur le socket "sock" et le 6 est le nombre maximal de connexion en attente*/




    pthread_t threads[NB_THREAD];

    int a[NB_THREAD];
    for (int i=0; i<NB_THREAD; i++){
        a[i] = i;
        pthread_create(&threads[i], NULL, thread_1(sock, server, buffer_rcv), &a[i]);
    }

    for (int i=0; i<NB_THREAD; i++){
        pthread_join(threads[i], NULL);
    }
}

void * thread_1(int sock, struct sockaddr_in server, char buffer_rcv[256]){

        struct sockaddr_in client;
        socklen_t size = sizeof(client);
        int csock = accept(sock, (struct sockaddr*)&client, &size);
        printf("%i",csock);

        getpeername(sock, (struct sockaddr*) &client, &size);
        printf("Connection from %s : %u\n", inet_ntoa(client.sin_addr), ntohs(server.sin_port));
        char connect = "Connecté";
        send (csock, connect, sizeof(connect),0);

        int boucle=1;
        while(boucle==1){
            recv(csock, buffer_rcv, sizeof(* buffer_rcv), 0); 
            printf("%s", buffer_rcv);
            send(csock, buffer_rcv, sizeof(* buffer_rcv), 0);
        }   
}

int getPort (int argc, char * argv[]){
    if (argc < 2){
        printf("Missing argument port!\n");
        exit(EXIT_FAILURE);
    }
    return atoi(argv[1]);
}
